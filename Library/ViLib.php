<?php
require_once 'Console/Table.php';

class Helper
{
    public static function isInt($value)
    {
        return strval($value) === strval(intval($value));
    }

    public static function inputInt($instruction = "")
    {
        $hasRun = false;
        do {
            if ($hasRun) {
                echo "* The value to enter must be an integer!\n";
            }
            $value = readline($instruction);
            $hasRun = true;
        } while (!self::isInt($value));
        return $value;
    }

    public static function inputIntRange($instruction = "", $min = null, $max = null)
    {
        $hasRun = false;
        do {
            if ($hasRun) {
                if ($max === null) {
                    echo "* The value to enter must be a positive integer!\n";
                } else {
                    echo "* Enter value must be an integer in the range from $min to $max!\n";
                }
            }
            $value = self::inputInt($instruction);
            if ($max !== null) {
                $checker = ($value < $min || $value > $max) ? true : false;
            } else {
                $checker = $value < 0 ? true : false;
            }
            $hasRun = true;
        } while ($checker);
        return $value;
    }

    public static function inputStrLimit($min, $max, $instruction = "")
    {
        $hasRun = false;
        do {
            if ($hasRun) {
                echo "* The string must be between $min and $max in length!\n";
            }
            $value = trim(readline($instruction), " \0\t\n\x0B\r");
            $hasRun = true;
        } while (strlen($value) < $min || strlen($value) > $max);
        return $value;
    }

    public static function inputStrNonPatter($pattern, $min, $max, $instruction = "")
    {
        $hasRun = false;
        do {
            if ($hasRun) {
                echo "* String can't contain spaces!\n";
            }
            $value = self::inputStrLimit($min, $max, $instruction);
            $hasRun = true;
        } while (preg_match($pattern, $value));
        return $value;
    }

    public static function confirm($message, $color = 39)
    {
        echo self::changeColorText($message, $color) . "\n";
        $input = readline("Enter 'y/Y' (Yes) to confirm or any character to skip: ");
        echo "\n";
        return strtolower($input) === 'y' ? true : false;
    }

    public static function changeColorText($text, $codeColor = 34)
    {
        return "\033[" . $codeColor . "m" . $text . "\033[0m";
    }

    public static function textSuccess($text)
    {
        return self::changeColorText($text, 32);
    }

    public static function textDanger($text)
    {
        return self::changeColorText($text, 31);
    }

    public static function textWarning($text)
    {
        return self::changeColorText($text, 33);
    }

    public static function textInfo($text)
    {
        return self::changeColorText($text, 36);
    }

    public static function boldText($text)
    {
        return "\033[1m" . $text . "\033[22m";
    }

    public static function decorateTitle($title, $char = '_', $length = 120, $codeColor = 34)
    {
        $lenTitle = strlen($title);
        $title = strtoupper($title);
        if ($length < $lenTitle) {
            return $title . "\n";
        } else {
            $lenSide = intdiv($length - $lenTitle, 2) - 1;
            if ($lenSide >= 0) {
                $side = str_repeat($char, $lenSide);
                $odd = ($length - $lenTitle) % 2;
                $result = $side . ' ' . self::boldText($title) . ' ' . $side . ($odd == 1 ? $char : '');
            } else {
                return $title . "\n";
            }
        }
        return self::changeColorText($result . "\n", $codeColor);
    }

    public static function packTitle($title, $char = '/', $length = 120, $codeColor = 34)
    {
        $lenTitle = strlen($title);
        $title = strtoupper($title);
        $result = '';

        $result .= str_repeat($char, $length) . "\n";
        if ($length < $lenTitle) {
            $length = $lenTitle + 2;
        }

        $size = intdiv($length - $lenTitle, 2) - 1;
        if ($size >= 0) {
            $side = str_repeat(' ', $size);
            $odd = ($length - $lenTitle) % 2;
            $result .= $char . $side . self::boldText($title, $codeColor) . $side . ($odd == 1 ? ' ' : '') . $char . "\n";
        } else {
            $result .= $title . "\n";
        }

        $result .= str_repeat($char, $length) . "\n";
        return self::changeColorText($result, $codeColor);
    }

    public static function printTable($array, $headers)
    {
        $tbl = new Console_Table();
        $tbl->setHeaders($headers);
        foreach ($array as $item) {
            $tbl->addRow($item);
        }

        echo $tbl->getTable();
    }

    public static function showMenuManager($nameItem, $extendMenu = [])
    {
        echo self::decorateTitle($nameItem . ' Management', '-') .
            "1. Add $nameItem\n" .
            "2. Edit $nameItem\n" .
            "3. Delete $nameItem\n" .
            "4. Show list\n";
        $index = 5;
        foreach ($extendMenu as $nameFunc) {
            echo "$index. $nameFunc\n";
            $index++;
        }
        echo "0. Exit\n";
    }
}