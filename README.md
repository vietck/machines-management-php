## LƯU Ý TRƯỚC KHI CHẠY CHƯƠNG TRÌNH

Vì chương trình có sử dụng một gói thư viện PHP bên thứ 3 để tạo và in các bảng dữ liệu nên máy tính muốn chạy chương trình này cần phải cài đặt gói thư viện đó thì mới có thể hoạt động được.

Vui lòng mở một cửa sổ Terminal và chạy các lệnh dưới đây để thực hiện việc cài đặt thư viện trên Ubuntu OS:

`sudo apt install php-pear`

`sudo pear channel-update pear.php.net`

`sudo pear install Console_Table`

### Todo
* Kho:
    * Nhập, xuất, chỉnh sửa thông tin kho.
    * Tính tổng tiền và tổng khối lượng của kho.
    * Tìm kiếm, thống kê (có thể sắp xếp theo các thuộc tính của máy).
    
* Máy:
    * Nhập, xuất, chỉnh sửa thông tin máy.
    * Tính tiền và khối lượng máy.
    
* Chi tiết phức và chi tiết đơn:
    * Nhập, xuất, chỉnh sửa thông tin chi tiết.
    * Tính tiền và khối lượng chi tiết.

### Check list
* Thuộc tính ID của mọi đối tượng là bắt buộc và không chứa ký tự khoảng trống.
* Giá tiền, khối lượng của chi tiết đơn là bắt buộc, là số nguyên và không được âm.
* Thuộc tính có giá trị kiểu chuỗi không được để trống và được giới hạn số lượng ký tự nhập vào.
* Giá trị để lựa chọn chức năng không để trống, là số nguyên và được giới hạn tương ứng với số chức năng của bảng chức năng đó.
* Thứ tự của đối tượng được chọn để tương tác được giới hạn theo số lượng danh sách đối tượng đó.
