<?php
require_once 'Model/SingleDetail.php';
require_once 'Model/ComplexDetail.php';
require_once 'Model/Machine.php';
require_once 'Model/Warehouse.php';
require_once 'Manager/SDetailManager.php';
require_once 'Manager/CDetailManager.php';
require_once 'Manager/MachineManager.php';
require_once 'Manager/WarehouseManager.php';

class MainProgram
{
    private $warehouseManager;
    private $machineManager;
    private $cDetailManager;
    private $sDetailManager;
    private $option = 1;

    public function __construct()
    {
        $this->sDetailManager = new SDetailManager(new SingleDetail(), 'single detail');
        $this->cDetailManager = new CDetailManager(new ComplexDetail(), 'complex detail', $this->sDetailManager);
        $this->machineManager = new MachineManager(new Machine(), 'machine', $this->cDetailManager);
        $this->warehouseManager = new WarehouseManager(new Warehouse(), 'warehouse', $this->machineManager, ['Search', 'Statistic']);
    }

    public function run()
    {
        echo Helper::packTitle('Machine Management Version 1.0', '/', 120, 33) . "\n";

        while ($this->option !== 0) {
            echo Helper::decorateTitle('Menu item', '=') .
                "1. Single detail\n" .
                "2. Complex detail\n" .
                "3. Machine\n" .
                "4. Warehouse\n" .
                "0. Exit\n";

            $this->option = Helper::inputIntRange('-> Choose item: ', 0, 4);
            echo "\n";

            switch ($this->option) {
                case 1:
                    $this->sDetailManager->selectFunc();
                    break;

                case 2:
                    $this->cDetailManager->selectFunc();
                    break;

                case 3:
                    $this->machineManager->selectFunc();
                    break;

                case 4:
                    $this->warehouseManager->selectFunc();
                    break;

                case 0:
                    $this->option = Helper::confirm('Do you want to exit the program?', 33) ? 0 : -1;
                    break;

                default:
                    echo "Function doesn't exist!\n";
            }
        }
    }
}

$mainProgram = new MainProgram();
$mainProgram->run();