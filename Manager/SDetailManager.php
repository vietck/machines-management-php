<?php
require_once "Manager.php";

class SDetailManager extends Manager {
     public function getArray()
    {
        $array = [];

        if ($this->lItem === []) {
            return $array;
        }

        foreach ($this->lItem as $item) {
            $array[] = [
                $item->getID(),
                $item->getPrice(),
                $item->getWeight()
            ];
        }
        return $array;
    }
}