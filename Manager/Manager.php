<?php
require_once "./Library/ViLib.php";

abstract class Manager
{
    public $nameItem;
    public $childManager;
    protected $item;
    protected $lItem = [];
    protected $extendMenu = [];
    protected $option = -1;

    public function __construct($item, $nameItem, $childManager = null, $extendMenu = [])
    {
        $this->item = $item;
        $this->nameItem = $nameItem;
        $this->childManager = $childManager;
        $this->extendMenu = $extendMenu;
    }

    abstract protected function getArray();

    public function show()
    {
        echo Helper::decorateTitle($this->nameItem, '~');
        Helper::printTable($this->getArray(),
            [
                'ID',
                'Price',
                'Weight'
            ]);
    }

    public function add()
    {
        $newItem = clone $this->item;
        $newItem->import($this);
        $this->lItem[] = $newItem;
    }

    public function handleFunc($nameFunc) {
        if ($this->lItem == []) {
            echo Helper::textDanger("* List is empty!\n\n");
            return;
        } else {
            while (true) {
                echo Helper::boldText("=> Choose $this->nameItem to $nameFunc:\n") .
                    Helper::textInfo("? Enter -1 to show list or 0 to quit.\n");
                $input = Helper::inputIntRange("-> Order of $this->nameItem: ", -1, count($this->lItem));
                echo "\n";
                if ($input == 0) {
                    return;
                } else if ($input == -1) {
                    $this->show();
                    echo "\n";
                } else {
                    if ($nameFunc == 'edit') {
                        $this->lItem[$input - 1]->edit($this);
                    } if ($nameFunc == 'delete') {
                        if (Helper::confirm("Do you want to delete this $this->nameItem?", 33)) {
                            array_splice($this->lItem, $input - 1, 1);
                            echo Helper::textSuccess("* Delete Successfully!\n\n");
                        }
                    }
                }
            }
        }
    }

    public function find($id)
    {
        foreach ($this->lItem as $item) {
            if ($item->getID() === $id) {
                return $item;
            }
        }
        return null;
    }

    public function getItem($index)
    {
        if ($this->lItem !== [] && $index > -1 && $index < count($this->lItem)) {
            return $this->lItem[$index];
        }
        return null;
    }

    public function getSize()
    {
        return count($this->lItem);
    }

    public function selectFunc()
    {
        $this->option = -1;
        while ($this->option !== 0) {
            Helper::showMenuManager($this->nameItem, $this->extendMenu);
            $this->option = Helper::inputIntRange('-> Choose: ', 0, 4);
            echo "\n";

            switch ($this->option) {
                case 1:
                    $this->add();
                    echo "\n";
                    break;

                case 2:
                    $this->handleFunc('edit');
                    break;

                case 3:
                    $this->handleFunc('delete');
                    break;

                case 4:
                    $this->show();
                    echo "\n";
                    break;

                case 0:
                    $this->option = 0;
                    break;

                default:
                    echo "Function doesn't exist!\n";
            }
        }
    }
}