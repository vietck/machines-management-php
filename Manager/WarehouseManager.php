<?php
require_once "Manager.php";

class WarehouseManager extends Manager
{
    protected function getArray()
    {
        $array = [];

        if ($this->lItem === []) {
            return $array;
        }

        foreach ($this->lItem as $item) {
            $array[] = [
                $item->getName(),
                $item->getPrice($this->childManager),
                $item->getWeight($this->childManager)
            ];
        }
        return $array;
    }

    public function show()
    {
        echo Helper::decorateTitle($this->nameItem, '~');
        Helper::printTable($this->getArray(),
            [
                'Name',
                'Total Price',
                'Total Weight'
            ]);
    }

    public function handleFuncWarehouse($nameFunc) {
        if ($this->lItem == []) {
            echo Helper::textDanger("* List is empty!\n\n");
            return;
        } else {
            while (true) {
                echo Helper::boldText("=> Choose $this->nameItem to $nameFunc:\n") .
                    Helper::textInfo("? Enter -1 to show list or 0 to quit.\n");
                $input = Helper::inputIntRange("-> Order of $this->nameItem: ", -1, count($this->lItem));
                echo "\n";

                if ($input == 0) {
                    return;
                } else if ($input == -1) {
                    $this->show();
                    echo "\n";
                } else {
                    if ($nameFunc == 'search') {
                        $this->lItem[$input - 1]->search($this->childManager);
                    } if ($nameFunc == 'statistic') {
                        $this->lItem[$input - 1]->statistic($this->childManager);
                    }
                }
            }
        }
    }

    public function selectFunc()
    {
        $this->option = -1;
        while ($this->option !== 0) {
            Helper::showMenuManager($this->nameItem, $this->extendMenu);
            $this->option = Helper::inputIntRange('-> Choose: ', 0, 6);
            echo "\n";

            switch ($this->option) {
                case 1:
                    $this->add();
                    echo "\n";
                    break;

                case 2:
                    $this->handleFunc('edit');
                    break;

                case 3:
                    $this->handleFunc('delete');
                    break;

                case 4:
                    $this->show();
                    echo "\n";
                    break;

                case 5:
                    $this->handleFuncWarehouse('search');
                    break;

                case 6:
                    $this->handleFuncWarehouse('statistic');
                    break;

                case 0:
                    $this->option = 0;
                    break;

                default:
                    echo "Function doesn't exist!\n";
            }
        }
    }
}