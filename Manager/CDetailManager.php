<?php
require_once "Manager.php";

class CDetailManager extends Manager {
    public function getArray()
    {
        $array = [];

        if ($this->lItem === []) {
            return $array;
        }

        foreach ($this->lItem as $item) {
            $array[] = [
                $item->getID(),
                $item->getPrice($this),
                $item->getWeight($this)
            ];
        }
        return $array;
    }
}