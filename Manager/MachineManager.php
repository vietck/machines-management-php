<?php
require_once "Manager.php";

class MachineManager extends Manager {
    protected function getArray()
    {
        $array = [];

        if ($this->lItem === []) {
            return $array;
        }

        foreach ($this->lItem as $item) {
            $array[] = [
                $item->getID(),
                $item->getName(),
                $item->getPrice($this->childManager),
                $item->getWeight($this->childManager)
            ];
        }
        return $array;
    }

    public function show()
    {
        echo Helper::decorateTitle($this->nameItem, '~');
        Helper::printTable($this->getArray(),
            [
                'ID',
                'Name',
                'Price',
                'Weight'
            ]);
    }
}