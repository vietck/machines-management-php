<?php
require_once './Library/ViLib.php';

abstract class Detail {
    protected $id;

    public function setID($id) {
        $this->id = $id;
    }

    public function getID() {
        return $this->id;
    }

    public function import() {
        $this->id = Helper::inputStrNonPatter('/\s/', 1, 20, '-> ID Detail: ');
    }

    public function export() {
        echo "ID: $this->id\n";
    }

    abstract public function getPrice();

    abstract public function getWeight();

    abstract public function edit(&$manager);
}