<?php
require_once 'Detail.php';
require_once 'HelperDetail.php';

class ComplexDetail extends Detail
{
    private $lDetails = [];

    public function getPrice(&$cDetailManager = null)
    {
        return HelperDetail::calSumDetail($cDetailManager, $this->lDetails);
    }

    public function getWeight(&$cDetailManager = null)
    {
        return HelperDetail::calSumDetail($cDetailManager, $this->lDetails, 'weight');
    }

    public function import(&$manager = null)
    {
        echo Helper::boldText("=> Enter data for complex detail:\n");
        parent::import();
        HelperDetail::addDetails($manager, $this->lDetails);
    }

    public function export(&$manager = null)
    {
        parent::export();
        echo    "Price: $this->getPrice($manager)\n" .
                "Weight: $this->getWeight($manager)\n";
    }

    public function edit(&$manager)
    {
        $option = -1;
        while ($option !== 0) {
            echo Helper::decorateTitle('Edit Complex Detail') .
                "1. Edit ID\n" .
                "2. Edit details list\n" .
                "0. Exit\n";
            $option = Helper::inputIntRange('=> Choose: ', 0, 2);
            echo "\n";

            switch ($option) {
                case 1:
                    parent::import();
                    echo "\n";
                    break;

                case 2:
                    HelperDetail::editDetails($manager,
                        $this->lDetails, $manager->nameItem);
                    echo "\n";
                    break;

                case 0:
                    $option = 0;
                    break;

                default:
                    echo "Function doesn't exist!\n";
            }
        }
    }
}