<?php
require_once './Library/ViLib.php';

class HelperDetail
{
    public static function getDetail($cDetailManager, $item)
    {
        $detail = null;
        if ($item[0] === 'CDetailManager') {
            $detail = $cDetailManager->find($item[1]);
        } else if ($item[0] === 'SDetailManager') {
            $detail = $cDetailManager->childManager->find($item[1]);
        }
        return $detail;
    }

    public static function calSumDetail($cDetailManager, $lDetails, $property = null)
    {
        $total = 0;
        foreach ($lDetails as $item) {
            $detail = self::getDetail($cDetailManager, $item);
            if ($detail) {
                $total += ($property === null ? $detail->getPrice($cDetailManager) : $detail->getWeight($cDetailManager));
            }
        }
        return $total;
    }

    public static function getField($manager, $list, $index, $field)
    {
        if ($field == 1) {
            return $list[$index];
        } else if ($field == 2) {
            return $manager->find($list[$index])->getName();
        } else if ($field == 3) {
            return $manager->find($list[$index])->getPrice($manager->childManager);
        } else if ($field == 4) {
            return $manager->find($list[$index])->getWeight($manager->childManager);
        }
        return 0;
    }

    public static function quickSort($manager, $list, $field, $direction)
    {
        if (count($list) <= 1) {
            return $list;
        } else {
            $pivot = self::getField($manager, $list, 0, $field);
            $left = [];
            $right = [];
            for ($i = 1; $i < count($list); $i++) {
                if ($direction == 1 ? self::getField($manager, $list, $i, $field) < $pivot : self::getField($manager, $list, $i, $field) > $pivot) {
                    $left[] = $list[$i];
                } else {
                    $right[] = $list[$i];
                }
            }
            return array_merge(self::quickSort($manager, $left, $field, $direction), [$list[0]], self::quickSort($manager, $right, $field, $direction));
        }
    }

    public static function getArray(&$cDetailManager, &$list)
    {
        $array = [];
        if ($list === []) {
            return $array;
        }

        foreach ($list as $item) {
            $detail = self::getDetail($cDetailManager, $item);
            if ($detail) {
                $array[] = [$detail->getID(), $detail->getPrice($cDetailManager), $detail->getWeight($cDetailManager)];
            }
        }
        return $array;
    }

    public static function showDetails(&$cDetailManager, &$list, &$nameItem)
    {
        echo Helper::decorateTitle("Details of $nameItem", '~');
        Helper::printTable(self::getArray($cDetailManager, $list),
            [
                'ID',
                'Price',
                'Weight'
            ]);
    }

    public static function addDetailsByType(&$detailManager, &$list)
    {
        while (true) {
            echo Helper::boldText("=> Enter order of $detailManager->nameItem in the list:\n") .
                Helper::textInfo("? Enter -1 to show $detailManager->nameItem list or 0 to quit.\n");
            $input = Helper::inputIntRange('-> Order of detail: ', -1, $detailManager->getSize());
            echo "\n";
            if ($input == 0) {
                return;
            } else if ($input == -1) {
                $detailManager->show();
                echo "\n";
            } else {
                $list[] = [get_class($detailManager), $detailManager->getItem($input - 1)->getID()];
                echo Helper::textSuccess("* Add Successfully!\n\n");
            }
        }
    }

    public static function addDetails(&$cDetailManager, &$list)
    {
        $option = -1;
        while ($option !== 0) {
            echo Helper::decorateTitle('Type Detail') .
                "1. Single Detail\n" .
                "2. Complex Detail\n" .
                "0. Exit\n";
            $option = Helper::inputIntRange('=> Choose: ', 0, 2);
            echo "\n";

            switch ($option) {
                case 1:
                    self::addDetailsByType($cDetailManager->childManager, $list);
                    break;

                case 2:
                    self::addDetailsByType($cDetailManager, $list);
                    break;

                case 0:
                    $option = 0;
                    break;

                default:
                    echo "Function doesn't exist!\n";
            }
        }
    }

    public static function removeDetails(&$cDetailManager, &$list, &$nameItem)
    {
        if ($list == []) {
            echo Helper::textDanger("* List is empty!\n\n");
            return;
        } else {
            while (true) {
                echo Helper::boldText("=> Choose detail to remove:\n") .
                    Helper::textInfo("? Enter -1 to show details list or 0 to quit.\n");
                $pos = Helper::inputIntRange('-> Order detail: ', -1, count($list));
                echo "\n";
                if ($pos == 0) {
                    return;
                } else if ($pos == -1) {
                    self::showDetails($cDetailManager, $list, $nameItem);
                } else {
                    array_splice($list, $pos - 1, 1);
                    echo Helper::textSuccess("* Removed successfully!\n\n");
                }
            }
        }
    }

    public static function editDetails(&$cDetailManager, &$list, &$nameItem)
    {
        $option = -1;
        while ($option !== 0) {
            echo Helper::decorateTitle("Edit Details of $nameItem") .
                "1. Show list\n" .
                "2. Add details\n" .
                "3. Remove details\n" .
                "0. Exit\n";

            $option = Helper::inputIntRange('=> Choose: ', 0, 3);
            echo "\n";

            switch ($option) {
                case 1:
                    self::showDetails($cDetailManager, $list, $nameItem);
                    echo "\n";
                    break;

                case 2:
                    self::addDetails($cDetailManager, $list);
                    break;

                case 3:
                    self::removeDetails($cDetailManager, $list, $nameItem);
                    break;

                case 0:
                    $option = 0;
                    break;

                default:
                    echo "Function does not exist!\n";
            }
        }
    }
}