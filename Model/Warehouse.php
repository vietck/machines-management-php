<?php
require_once 'HelperDetail.php';

class Warehouse
{
    private $name = "";
    private $lMachines = [];

    private function enterName()
    {
        $this->name = Helper::inputStrLimit(1, 30, '-> Name warehouse: ');
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPrice(&$machineManager)
    {
        $total = 0;
        foreach ($this->lMachines as $id) {
            $total += $machineManager->find($id)->getPrice($machineManager->childManager);
        }
        return $total;

    }

    public function getWeight(&$machineManager)
    {
        $total = 0;
        foreach ($this->lMachines as $id) {
            $total += $machineManager->find($id)->getWeight($machineManager->childManager);
        }
        return $total;
    }

    public function getArray(&$machineManager)
    {
        $array = [];
        if ($this->lMachines === []) {
            return $array;
        }

        foreach ($this->lMachines as $id) {
            $machine = $machineManager->find($id);
            if ($machine) {
                $array[] = [
                    $machine->getID(),
                    $machine->getName(),
                    $machine->getPrice($machineManager->childManager),
                    $machine->getWeight($machineManager->childManager)
                ];
            }
        }
        return $array;
    }

    public function showMachines(&$machineManager)
    {
        echo Helper::decorateTitle('Machines of Warehouse', '~');
        Helper::printTable($this->getArray($machineManager),
            [
                'ID',
                'Name',
                'Price',
                'Weight'
            ]);
    }

    private function addMachines(&$machineManager)
    {
        echo Helper::decorateTitle("Machines of Warehouse");
        while (true) {
            echo Helper::boldText("=> Enter order of $machineManager->nameItem in the list:\n") .
                Helper::textInfo("? Enter -1 to show $machineManager->nameItem list or 0 to quit.\n");
            $input = Helper::inputIntRange('-> Order machine: ', -1, $machineManager->getSize());
            echo "\n";
            if ($input == 0) {
                return;
            } else if ($input == -1) {
                $machineManager->show();
                echo "\n";
            } else {
                $this->lMachines[] = $machineManager->getItem($input - 1)->getID();
                echo Helper::changeColorText("* Add Successfully!\n\n", 32);
            }
        }
    }

    private function removeMachines(&$machineManager)
    {
        if ($this->lMachines === []) {
            echo Helper::textDanger("* List is empty!\n\n");
            return;
        } else {
            while (true) {
                echo Helper::boldText("=> Choose machine to remove:\n") .
                    Helper::textInfo("? Enter -1 to show machines list or 0 to quit.\n");
                $pos = Helper::inputIntRange('-> Order machine: ', -1, count($this->lMachines));
                echo "\n";

                if ($pos == 0) {
                    return;
                } else if ($pos == -1) {
                    $this->showMachines($machineManager);
                    echo "\n";
                } else {
                    array_splice($this->lMachines, $pos - 1, 1);
                    echo Helper::textSuccess("* Removed successfully!\n\n");
                }
            }
        }
    }

    public function import($manager)
    {
        echo "=> Enter data for warehouse:\n";
        $this->enterName();
        $this->addMachines($manager->childManager);
    }

    public function export($machineManager)
    {
        echo "Name: $this->getName()\n" .
            "Total Price: $this->getPrice($machineManager)\n" .
            "Total Weight: $this->getWeight($machineManager)\n";
    }

    public function editList($machineManager)
    {
        $option = -1;
        while ($option !== 0) {
            echo Helper::decorateTitle('Edit Machines list of Warehouse', '-') .
                "1. Show list\n" .
                "2. Add machines\n" .
                "3. Remove machines\n" .
                "0. Exit\n";

            $option = Helper::inputIntRange('=> Choose function: ', 0, 3);
            echo "\n";

            switch ($option) {
                case 1:
                    $this->showMachines($machineManager);
                    echo "\n";
                    break;

                case 2:
                    $this->addMachines($machineManager);
                    break;

                case 3:
                    $this->removeMachines($machineManager);
                    break;

                case 0:
                    $option = 0;
                    break;

                default:
                    echo "Function does not exist!\n";
            }
        }
    }

    public function edit($manager)
    {
        $option = -1;
        while ($option !== 0) {
            echo Helper::decorateTitle('Edit Warehouse', '-') .
                "1. Edit name\n" .
                "2. Edit machines list\n" .
                "0. Exit\n";
            $option = Helper::inputIntRange('=> Choose function: ', 0, 2);
            echo "\n";

            switch ($option) {
                case 1:
                    $this->enterName();
                    echo "\n";
                    break;

                case 2:
                    $this->editList($manager->childManager);
                    break;

                case 0:
                    $option = 0;
                    break;

                default:
                    echo "Function does not exist!\n";
            }
        }
    }

    public function search($machineManager)
    {
        $option = -1;
        while ($option !== 0) {
            echo Helper::decorateTitle('Search machines') .
                "1. All\n" .
                "2. By ID\n" .
                "3. By Name\n" .
                "0. Exit\n";
            $option = Helper::inputIntRange('-> Choose: ', 0, 3);
            echo "\n";

            if ($option == 0) {
                return;
            }

            $keyWord = Helper::inputStrLimit(0, 40, '-> Key word: ');
            echo "\n";

            $result = [];
            foreach ($this->lMachines as $id) {
                $machine = $machineManager->find($id);
                if ($machine->checkSearch($option, $keyWord)) {
                    $result[] = [
                        $machine->getID(),
                        $machine->getName(),
                        $machine->getPrice($machineManager->childManager),
                        $machine->getWeight($machineManager->childManager)
                    ];
                }
            }

            echo Helper::textInfo("* Search result:\n") .
                Helper::decorateTitle('Machines in Warehouse', '~');
            Helper::printTable($result,
                [
                    'ID',
                    'Name',
                    'Price',
                    'Weight'
                ]);
            echo "\n";
        }
    }

    public function statistic($machineManager)
    {
        echo Helper::decorateTitle('Sort machines') .
            "1. ID\n" .
            "2. Name\n" .
            "3. Price\n" .
            "4. Weight\n" .
            "0. Exit\n";
        $field = Helper::inputIntRange('=> Choose: ', 0, 4);
        echo "\n";

        if ($field == 0) {
            return;
        }

        echo Helper::decorateTitle('Sort direction') .
            "1. Ascending\n" .
            "2. Decrease\n" .
            "0. Exit\n";

        $direction = Helper::inputIntRange('=> Choose: ', 0, 2);
        echo "\n";

        if ($direction == 0) {
            return;
        }

        $this->lMachines = HelperDetail::quickSort($machineManager, $this->lMachines, $field, $direction);

        echo Helper::textInfo("* Sort result:\n");
        $this->showMachines($machineManager);
        echo "\n";
    }
}