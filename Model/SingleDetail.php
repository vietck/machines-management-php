<?php
require_once 'Detail.php';

class SingleDetail extends Detail {
    private $price;
    private $weight;

    public function enterPrice() {
        $this->price = Helper::inputIntRange('-> Price: ');
    }

    public function enterWeight() {
        $this->weight = Helper::inputIntRange('-> Weight: ');
    }

    public function import() {
        echo Helper::boldText("=> Enter data for single detail:\n");
        parent::import();
        $this->enterPrice();
        $this->enterWeight();
    }

    public function export() {
        parent::export();
        echo    "Price: $this->price\n" .
                "Weight: $this->price\n";
    }

    public function getPrice() {
        return $this->price;
    }

    public function getWeight() {
        return $this->weight;
    }

    public function edit(&$manager)
    {
        $option = -1;
        while ($option !== 0) {
            echo Helper::decorateTitle("Edit $manager->nameItem") .
                "1. Edit ID\n" .
                "2. Edit Price\n" .
                "3. Edit Weight\n" .
                "0. Exit\n";
            $option = Helper::inputIntRange('=> Choose: ', 0, 3);
            echo "\n";

            switch ($option) {
                case 1:
                    parent::import();
                    echo "\n";
                    break;

                case 2:
                    $this->enterPrice();
                    echo "\n";
                    break;

                case 3:
                    $this->enterWeight();
                    echo "\n";
                    break;

                case 0:
                    $option = 0;
                    break;

                default:
                    echo "Function doesn't exist!\n";
            }
        }
    }
}