<?php
require_once 'HelperDetail.php';
require_once './Library/ViLib.php';

class Machine
{
    private $id = "";
    private $name = "";
    private $lDetails = [];

    private function enterID()
    {
        $this->id = Helper::inputStrNonPatter('/\s/', 1, 30, '-> ID Machine: ');
    }

    public function getID()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    private function enterName()
    {
        $this->name = Helper::inputStrLimit(1, 30, '-> Name machine: ');
    }

    public function getPrice(&$cDetailManager = null)
    {
        return HelperDetail::calSumDetail($cDetailManager, $this->lDetails);
    }

    public function getWeight(&$cDetailManager = null)
    {
        return HelperDetail::calSumDetail($cDetailManager, $this->lDetails, 'weight');
    }

    public function import(&$manager)
    {
        echo Helper::boldText("=> Enter data for machine:\n");
        $this->enterID();
        $this->enterName();
        HelperDetail::addDetails($manager->childManager, $this->lDetails);
    }

    public function export(&$manager)
    {
        echo    "ID: $this->id\n" .
                "Name: $this->name\n" .
                "Price: $this->getPrice($manager->childManager)\n" .
                "Weight: $this->getWight($manager->childManager)\n";
    }

    public function editProperty()
    {
        $option = -1;
        while ($option !== 0) {
            echo Helper::decorateTitle('Edit Property of Machine') .
                "1. Edit ID\n" .
                "2. Edit Name\n" .
                "0. Exit\n";
            $option = Helper::inputIntRange('=> Choose: ', 0, 2);
            echo "\n";

            switch ($option) {
                case 1:
                    $this->enterID();
                    echo "\n";
                    break;

                case 2:
                    $this->enterName();
                    echo "\n";
                    break;

                case 0:
                    $option = 0;
                    break;

                default:
                    echo "Function doesn't exist!\n";
            }
        }
    }

    public function edit(&$manager = null)
    {
        $option = -1;
        while ($option !== 0) {
            echo Helper::decorateTitle('Edit Machine') .
                "1. Edit property\n" .
                "2. Edit details list\n" .
                "0. Exit\n";
            $option = Helper::inputIntRange('=> Choose: ', 0, 2);
            echo "\n";

            switch ($option) {
                case 1:
                    $this->editProperty();
                    break;

                case 2:
                    HelperDetail::editDetails($manager->childManager,
                        $this->lDetails, $manager->nameItem);
                    break;

                case 0:
                    $option = 0;
                    break;

                default:
                    echo "Function doesn't exist!\n";
            }
        }
    }

    public function checkSearch($scope = 0, $keyWord = '')
    {
        if ($scope == 1) {
            if ((stristr($this->id, $keyWord) !== false) || (stristr($this->name, $keyWord) !== false))
                return true;
        } else if ($scope == 2) {
            if (stristr($this->id, $keyWord) !== false)
                return true;
        } else if ($scope == 3) {
            if (stristr($this->name, $keyWord) !== false)
                return true;
        }
        return false;
    }
}